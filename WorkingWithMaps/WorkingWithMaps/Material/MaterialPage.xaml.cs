﻿using System.Diagnostics;
using Xamarin.Forms;
using Xamarin.Forms.Maps;

namespace WorkingWithMaps.Material
{
    public partial class MaterialPage : ContentPage
    {
        public MaterialPage()
        {
            InitializeComponent();
        }

        void OnMapClicked(object sender, MapClickedEventArgs e)
        {
            Debug.WriteLine($"MapClick: {e.Position.Latitude}, {e.Position.Longitude}");
        }
    }
}
