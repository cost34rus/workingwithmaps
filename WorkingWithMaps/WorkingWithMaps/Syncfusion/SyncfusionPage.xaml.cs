﻿using System;
using System.Diagnostics;
using Xamarin.Forms;
using Xamarin.Forms.Maps;

namespace WorkingWithMaps.Syncfusion
{
    public partial class SyncfusionPage : ContentPage
    {
        public SyncfusionPage()
        {
            InitializeComponent();
        }

        void OnMapClicked(object sender, MapClickedEventArgs e)
        {
            Debug.WriteLine($"MapClick: {e.Position.Latitude}, {e.Position.Longitude}");
        }

        private void Button_Clicked(object sender, EventArgs e)
        {
            datePicker.IsOpen = true;
        }
    }
}
