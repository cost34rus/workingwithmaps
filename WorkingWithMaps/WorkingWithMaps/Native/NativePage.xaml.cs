﻿using System.Diagnostics;
using Xamarin.Forms;
using Xamarin.Forms.Maps;

namespace WorkingWithMaps.Native
{
    public partial class NativePage : ContentPage
    {
        public NativePage()
        {
            InitializeComponent();
        }

        void OnMapClicked(object sender, MapClickedEventArgs e)
        {
            Debug.WriteLine($"MapClick: {e.Position.Latitude}, {e.Position.Longitude}");
        }
    }
}
